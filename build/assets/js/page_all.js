/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2023. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggler').on('click', function () {
    $(this).toggleClass('active');
    $('.navbar-collapse').toggleClass('show');
  });
  $('.navbar-collapse .close').on('click', function () {
    $('.navbar-collapse').removeClass('show');
  });
  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $('header').addClass('scroll');
    } else {
      $('header').removeClass('scroll');
    }
  });

  // active navbar of page current
  var urlcurrent = window.location.pathname;
  $(".navbar li a[href$='" + urlcurrent + "'],.dashboard__nav li a[href$='" + urlcurrent + "']").addClass('active');
});